# README #

# Sodelujoči#
* Benjamin Medvešček Murovec

# Povzetek #
* Igra "Tower Defence"
* Postavljaš stolpe, ki streljajo nasprotnike, ki ne smejo priti do konca

# Opis #
* S tipko "s" pošljemo enega nasprotnika po poti.
* Z levim miškinim klikom postavimo stolp.

# To Do#
* Naredi več stolpov, in GUI, kjer se jih lahko menja.
* Naredi več nasprotnikov.
* Naredi nekaj različnih stopenj in poti za nasprotnike.