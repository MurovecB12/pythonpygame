import pygame
import nasprotnik
import Turret 
import os




def program():
    pygame.init()

    platnoDim = (1000,800)

    platno = pygame.display.set_mode(platnoDim)
    ozadje = pygame.Surface(platnoDim)
    barva_ozadja = (1,100,50)
    ozadje.fill(barva_ozadja)
    ura = pygame.time.Clock()
    FPS = 40
    platno.fill(barva_ozadja)
    pygame.display.update()
    count_hud = 0
    hudobci = {}
    tureti = []
    pot = [(0,400),(100,400),(800,400),(800,780),(900,780),(900,10)]
    izhod = False

    zbrisi_hud_indeks = []

    cesta = Turret.Cesta(pot,20)
    cesta.narisi_cesto(ozadje,cesta.sirina)
    platno.blit(ozadje,(0,0))
    pygame.display.update()
    

    slime = pygame.image.load("Slime.png")
    pygame.display.update()

    #DISPLAY TEXT
    basicfont = pygame.font.SysFont(None, 48)
    #text = basicfont.render('Hello World!', True, (255, 0, 0), barva_ozadja)
    #platno.blit(text, pygame.Rect(100,20,200,50))
    pygame.display.update()

    st_zivljenj = 100
    sprememba_zivljenj = True
    while not izhod:
        dirty_rects = []
        
        
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT: #ko pritisnemo križec
                izhod = True
            if event.type == pygame.KEYDOWN and (event.key == pygame.K_d or event.key == pygame.K_s):
                hudobci[count_hud] = (nasprotnik.Hudobec(count_hud, pot,10))
                count_hud += 1
            if event.type == pygame.MOUSEBUTTONDOWN:
                postavi = True
                miska = pygame.mouse.get_pos()
                for t in tureti:
                    if t.rect.colliderect(pygame.Rect((miska[0]-15,miska[1]-15),(30,30))):
                        postavi = False
                if postavi:
                    tureti.append(Turret.Turret(miska,100,10))
                    turet = tureti[-1]
                    ozadje.blit(turet.doseg_slika,turet.doseg)
                    ozadje.blit(turet.slika,(turet.pozicija[0]-15,turet.pozicija[1]-15))
                    platno.blit(ozadje,(turet.pozicija[0]-turet.polmer/2,turet.pozicija[1]-turet.polmer/2),area=turet.doseg)
                    pygame.display.update()
                
        TuretRects = [] 
        for h in hudobci.keys():
            hud = hudobci[h]
            for turet in tureti:
                if turet.reload == 10:
                    turet.reload = 0
                    turet.ustreli(hud)
                    
                    
            if hud.ziv:
                platno.fill(barva_ozadja,hud.rect) #zbrišem
                platno.blit(ozadje,hud.rect,hud.rect)
                dirty_rects.append(hud.rect)      #dodam za posodobit
                hud.premakni()                   #premaknem hudobca
                platno.blit(hud.slika,hud.pozicija) #narišem hudobca
                dirty_rects.append(hud.rect)      #dodan še za posodobit
                if pot[-1][0]-5 <= hud.pozicija[0] <= pot[-1][0]+5 and pot[-1][1]-5 <= hud.pozicija[1] <= pot[-1][1] + 5:
                    st_zivljenj -=1
                    platno.fill(barva_ozadja,hud.rect)
                    platno.blit(ozadje,hud.rect,hud.rect)
                    dirty_rects.append(hud.rect)
                    zbrisi_hud_indeks.append(hud.identiteta)
                    sprememba_zivljenj = True
            if  not hud.ziv:
                platno.fill(barva_ozadja,hud.rect)
                platno.blit(ozadje,hud.rect,hud.rect)
                dirty_rects.append(hud.rect)
                zbrisi_hud_indeks.append(hud.identiteta)
        for turet in tureti:
            if turet.reload <10:
                turet.reload += 1
        for i in zbrisi_hud_indeks:
            del hudobci[i]
        zbrisi_hud_indeks = []

        if sprememba_zivljenj:
            text = basicfont.render("Stevilo zivljenj: "+str(st_zivljenj), True, (255, 0, 0), barva_ozadja)
            okvir_pisanja = pygame.Rect(100,20,330,30)
            platno.fill(barva_ozadja, okvir_pisanja)
            platno.blit(text, okvir_pisanja)
            dirty_rects.append(okvir_pisanja)
            sprememba_zivljenj = False
        
        ura.tick(FPS)
        pygame.display.update(dirty_rects)
        pygame.display.update(TuretRects)
        
    pygame.quit()
        
program()
