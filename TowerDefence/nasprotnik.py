import pygame
import os

def sestej_tuple(v1,v2):
    return (v1[0]+v2[0],v1[1]+v2[1])



class Hudobec():


    #galerijaSlik = {}
    #slime = pygame.image.load("Slime.png")
    #galerijaSlik["Slime"] = slime
    slime = pygame.image.load("Slime.png")
    def __init__(self,identiteta,pot,hitrost = 1 ,hp = 100 ,barva = (0,0,0),slika=slime):
 
        self.identiteta = identiteta
        self.pozicija = pot[0] #tuple
        self.pot = pot #Seznam vozlisc(checkpointou)
        self.hitrost = hitrost # piksli na sekundo
        self.koefs = [] # zeznam koeficientou za hitrost, med i in i+1 vozliščem
        self.odsek = 0
        self.barva = barva
        self.slika = slika           #galerijaSlik["Slime"]
        self.rect = pygame.Rect(self.pozicija,(10,10))
        self.hp = hp
        self.ziv = True

        for i in range(len(self.pot)-1):
            d = ((self.pot[i+1][0]-self.pot[i][0])**2 +(self.pot[i+1][1]-self.pot[i][1])**2 )**(1/2)
            x = ((self.pot[i+1][0]-self.pot[i][0])*self.hitrost)/d
            y = ((self.pot[i+1][1]-self.pot[i][1])*self.hitrost)/d
            self.koefs.append((x,y))

    def premakni(self):
        
        try:
            self.pozicija = sestej_tuple(self.pozicija,self.koefs[self.odsek])
            self.rect = (self.pozicija,(10,10))
            if self.pot[self.odsek+1][0]-1 <= self.pozicija[0] <= self.pot[self.odsek+1][0]+1 and self.pot[self.odsek+1][1]-1 <= self.pozicija[1] <= self.pot[self.odsek+1][1]+1:
                self.odsek += 1
        except IndexError:
            self.ziv = False

    def prejmi_skodo(self,dmg):
        self.hp -= dmg
        if self.hp <= 0:
            self.ziv = False
        
