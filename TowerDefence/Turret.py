import pygame
import nasprotnik
import os

class Turret:

    turet1 = pygame.image.load("Turet1.png")
    def __init__(self,pozicija,r,dmg,barva=(100,100,100),slika = turet1):
        self.pozicija = pozicija
        self.dmg = dmg
        self.barva = barva
        self.polmer = r
        self.doseg = pygame.Rect((self.pozicija[0]-r/2,self.pozicija[1]-r/2),(r,r))
        self.doseg_slika = pygame.Surface((r,r))
        self.doseg_slika.fill(self.barva)
        self.doseg_slika.set_alpha(100)
        self.slika = slika
        self.rect = pygame.Rect((self.pozicija[0]-15,self.pozicija[1]-15),(30,30))
        self.reload = 10

    def ustreli(self,hud):
        if self.doseg.colliderect(hud.rect) == 1 and hud.ziv:
            hud.prejmi_skodo(self.dmg)
            

            
class Cesta:

    def __init__(self,pot,sirina):
        self.pot = pot
        self.sirina = sirina
        self.barva = (221,221,221)

    def narisi_cesto(self,platno,sirina = 20):
        pot = self.pot
        for i in range(len(pot)-1):
            a=pot[i]
            b=pot[i+1]
            pygame.draw.line(platno,self.barva,a,b,sirina)
        
        
        
        
